# Avenue Code Java Challenge Configuration

### How to compile ###
mvn install 

### How to run JUnit Test ###
mvn clean test

### How to run Integration Test ###
mvn clean verify -P integration-test

### How to run using Jetty ###
mvn jetty:run


# My considerations #

- I decided to keep the project as simple as possible.

- As I am currently working and the time to do the test was very short, I decided to make the project running instead of making a complex architecture that could not work.

- I decided to create integration tests because I needed to see if the application would run properly. To configure Maven for the integration tests, I followed the link: http://www.petrikainulainen.net/programming/maven/integration-testing-with-maven/.


# Rest Classes

### ProductRestController.java

- public Product getProduct(@PathVariable Integer id) read a single product by its id
- public List<Product> getAllProducts() list product catalog
- public Integer saveProduct(@RequestBody Product product) Created only to populate the database, there was no requirements for this

### OrderRestController.java

- public Integer placeAnOrder(@RequestBody CustomerOrder order) place an order
- public CustomerOrder getOrder(@PathVariable Integer id) read an existing order by its id
- public List<CustomerOrder> getAllProducts() list placed orders
- public void modifyOrder(@RequestBody CustomerOrder order) modify and existing order

# Architecture
Due to the ease of understanding of the Architecture I believe it is not necessary further explanation about it, but of course I am completely available for answering any questions.
Moreover, there are many points in the architecture which can be improved depending on the requirements details.

Any questions, please contact me through sergiocf@gmail.com.

Thank you,

Sergio Fonseca. 