package com.avenuecide.sergio.challenge.integration.test.rest;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.avenuecode.sergio.challenge.entity.Product;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class ITTestProductRestController {
	
	private String endpoint = "http://localhost:8080";
	
	@Test
	public void testGetProduct() throws JsonParseException, JsonMappingException, IOException {		
		Product productLGLB6500 = new Product();
		productLGLB6500.setName("LG LB6500");
		productLGLB6500.setValue(1900.0);
		
		saveProduct(productLGLB6500);		
		
		Product product = getProduct(1);		
						
		Assert.assertNotNull("Product", product);
		assertEquals(product.getId().longValue(),productLGLB6500.getId().longValue());
		Assert.assertNotNull("Product Name", product.getName());
		Assert.assertNotNull("Product Value", product.getValue());		
		
	}
	
	@Test
	public void testGetAllProducts() throws JsonParseException, JsonMappingException, IOException {		
		Product product = new Product();
		product.setName("SAMSUNG");
		product.setValue(2300.0);
		
		saveProduct(product);
	
		List list = getAllProducts();
					
		Assert.assertTrue(list.size() > 0);			
		

	}
	
	private Product getProduct(Integer id) {
		return new RestTemplate().getForObject(endpoint + "/resource/products/{id}", Product.class, id);		
	}
	
	@SuppressWarnings("unchecked")
	private List<Product> getAllProducts() {
		return new RestTemplate().getForObject(endpoint + "/resource/products/", List.class, "");		
	}
	
	private Product saveProduct(Product product) {		
		Integer id = new RestTemplate().postForObject(endpoint + "/resource/products", product, Integer.class);
		product.setId(id);
		
		return product;
	}

}
