package com.avenuecide.sergio.challenge.integration.test.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.avenuecode.sergio.challenge.entity.ItemOrder;
import com.avenuecode.sergio.challenge.entity.CustomerOrder;
import com.avenuecode.sergio.challenge.entity.Product;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class ITTestOrderRestController {
	private String endpoint = "http://localhost:8080";
		
	@Test
	public void tests() throws JsonParseException, JsonMappingException, IOException{
		testPlaceAnOrder();
		CustomerOrder order = testGetOrder();	
		testAddItemOrder(order);
		testGetAllProducts();
	}
		
	public void testAddItemOrder(CustomerOrder order) throws JsonParseException, JsonMappingException, IOException {
		Product productSamsung = new Product();
		productSamsung.setId(2);
		
		ItemOrder itemOrder = new ItemOrder();
		itemOrder.setOrder(order);
		itemOrder.setProduct(productSamsung);
		itemOrder.setAmount(2);
		
		order.getItems().add(itemOrder);		
		
		updateOrder(order);			
		order = getOrder(1);		
		
		Assert.assertTrue(order.getItems().size() > 1);		
		Assert.assertEquals(6500.0, order.getTotal(), 000.1);

	}

	public void testPlaceAnOrder() throws JsonParseException, JsonMappingException, IOException {
		CustomerOrder order = new  CustomerOrder();
		order.setEmail("sergiocf@gmail.com");
		order.setClient("Sergio Fonseca");
		order.setItems(new ArrayList<ItemOrder>());
		
		Product productLGLB6500 = new Product();
		productLGLB6500.setId(1);
		
		ItemOrder itemOrder = new ItemOrder();
		itemOrder.setOrder(order);
		itemOrder.setProduct(productLGLB6500);
		itemOrder.setAmount(1);
		
		order.getItems().add(itemOrder);
		
		order = saveOrder(order);

		Assert.assertTrue(order.getId() != null);		
	}	
	
	public CustomerOrder testGetOrder() throws JsonParseException, JsonMappingException, IOException {				
		CustomerOrder order = getOrder(1);		
								
		Assert.assertEquals(1900.0, order.getTotal(), 000.1);
		Assert.assertTrue(order.getItems().size() > 0);		
		
		return order;
	}
		
	public void testGetAllProducts() throws JsonParseException, JsonMappingException, IOException {		
		List<CustomerOrder> list = getAllOrders();		
		
		Assert.assertTrue(list.size() > 0);				
	}
		
	private CustomerOrder saveOrder(CustomerOrder order) {		
		Integer id = new RestTemplate().postForObject(endpoint + "/resource/orders", order, Integer.class);
		order.setId(id);
		
		return order;
	}
	
	private CustomerOrder getOrder(Integer id) {
		return new RestTemplate().getForObject(endpoint + "/resource/orders/{id}", CustomerOrder.class, id);		
	}
	
	@SuppressWarnings("unchecked")
	private List<CustomerOrder> getAllOrders() {
		return new RestTemplate().getForObject(endpoint + "/resource/orders/", List.class, "");		
	}
	
	private void updateOrder(CustomerOrder order) {		
		new RestTemplate().put(endpoint + "/resource/orders", order);
	}

}
