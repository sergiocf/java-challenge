package com.avenuecode.sergio.challenge.test.rest;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.avenuecode.sergio.challenge.entity.CustomerOrder;
import com.avenuecode.sergio.challenge.entity.ItemOrder;
import com.avenuecode.sergio.challenge.entity.Product;
import com.avenuecode.sergio.challenge.repository.OrderRepository;
import com.avenuecode.sergio.challenge.service.OrderService;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestOrderService {

	@InjectMocks
	private OrderService service;

	@Mock
	private OrderRepository repository;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
    @Test
    public void testTotalOrder() {        
        when(repository.getOrder(1)).thenReturn(getFakeOrder());        
        CustomerOrder order = service.getOrder(1);        
        assertThat(order.getTotal().compareTo(6500.0), is(0));
    }
    
    public CustomerOrder getFakeOrder() {
    	CustomerOrder order = new CustomerOrder();
    	order.setItems(new ArrayList<ItemOrder>());
    	
		Product productLGLB6500 = new Product();
		productLGLB6500.setName("LG LB6500");
		productLGLB6500.setValue(1900.0);
		
		Product productSamsung = new Product();
		productSamsung.setName("Smart TV LED 40 Full HD");
		productSamsung.setValue(2300.0);
		
		ItemOrder itemOrder = new ItemOrder();
		itemOrder.setOrder(order);
		itemOrder.setProduct(productLGLB6500);
		itemOrder.setAmount(1);
		order.getItems().add(itemOrder);

		
		itemOrder = new ItemOrder();
		itemOrder.setOrder(order);
		itemOrder.setProduct(productSamsung);
		itemOrder.setAmount(2);		
		order.getItems().add(itemOrder);
		
		return order;   	    	
    }

}
