package com.avenuecode.sergio.challenge.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class CustomerOrder implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private Integer id;
	
	private String client;
	
	private String email;
	
	@OneToMany(mappedBy="order", cascade={CascadeType.ALL}) 
	@JsonManagedReference
	private List<ItemOrder> items;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public List<ItemOrder> getItems() {
		return items;
	}

	public void setItems(List<ItemOrder> items) {
		this.items = items;
	}
	
	@JsonIgnore
	public Double getTotal() {
		Double sum = 0.0;
		for(ItemOrder item: items){
			sum += item.getTotal();
			
		}
		
		return sum;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", client=" + client + ", email=" + email + ", items=" + items + "]";
	}

}
