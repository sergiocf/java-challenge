package com.avenuecode.sergio.challenge.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class ItemOrder {	

	@Id
	@GeneratedValue
	private Integer id;
	
	@ManyToOne
	private Product product;
	
	private Integer amount;
	
	@JsonBackReference
	@ManyToOne
	private CustomerOrder order;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public CustomerOrder getOrder() {
		return order;
	}

	public void setOrder(CustomerOrder order) {
		this.order = order;
	}
	
	@JsonIgnore
	public Double getTotal(){
		return amount * product.getValue();
	}

	@Override
	public String toString() {
		return "ItemOrder [id=" + id + ", product=" + product + ", amount=" + amount + "]";
	}

}
