package com.avenuecode.sergio.challenge.repository;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.avenuecode.sergio.challenge.entity.Product;

@Repository
public class ProductRepository {

	@Autowired
	private SessionFactory sessionFactory;

	public Product getProduct(Integer id) {
		return (Product) sessionFactory.getCurrentSession().get(Product.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Product> getAllProducts() {
		return sessionFactory.getCurrentSession().createQuery("from Product").list();
	}
	
	public Integer saveProduct(Product product) {
		return (Integer)sessionFactory.getCurrentSession().save(product);
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
