package com.avenuecode.sergio.challenge.repository;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.avenuecode.sergio.challenge.entity.CustomerOrder;

@Repository
public class OrderRepository {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Integer saveOrder(CustomerOrder order) {
		return (Integer) sessionFactory.getCurrentSession().save(order);
	}

	public CustomerOrder getOrder(Integer id) {
		CustomerOrder order = (CustomerOrder) sessionFactory.getCurrentSession().get(CustomerOrder.class, id);
		Hibernate.initialize(order.getItems());
		return order;
	}

	@SuppressWarnings("unchecked")
	public List<CustomerOrder> getAllOrders() {
		return sessionFactory.getCurrentSession().createQuery("from CustomerOrder o join fetch o.items").list();
	}
	
	public void updateOrder(CustomerOrder order) {
		sessionFactory.getCurrentSession().update(order);
	}

}
