package com.avenuecode.sergio.challenge.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.sergio.challenge.entity.Product;
import com.avenuecode.sergio.challenge.service.ProductService;

@RestController
@RequestMapping("/resource/products")
public class ProductRestController {

	@Autowired
	private ProductService service;
	
	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" })
	public List<Product> getAllProducts() {
		return service.getAllProducts();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public Product getProduct(@PathVariable Integer id) {
		return service.getProduct(id);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = { "application/json" }, produces = { "application/json" })
	public Integer saveProduct(@RequestBody Product product) {
		return service.saveProduct(product);		
	}

}
