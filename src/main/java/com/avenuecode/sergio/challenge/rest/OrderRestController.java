package com.avenuecode.sergio.challenge.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.sergio.challenge.entity.CustomerOrder;
import com.avenuecode.sergio.challenge.service.OrderService;

@RestController
@RequestMapping("/resource/orders")
public class OrderRestController {
	
	@Autowired
	private OrderService service;
	
	@RequestMapping(method = RequestMethod.POST, consumes = { "application/json" }, produces = { "application/json" })
	public Integer placeAnOrder(@RequestBody CustomerOrder order) {
		return service.placeAnOrder(order);		
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public CustomerOrder getOrder(@PathVariable Integer id) {
		return service.getOrder(id);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" })
	public List<CustomerOrder> getAllProducts() {
		return service.getAllOrders();
	}
	
	@RequestMapping(method = RequestMethod.PUT, consumes = {"application/json" })
	public void modifyOrder(@RequestBody CustomerOrder order) {
		service.modifyOrder(order);		
	}

}
