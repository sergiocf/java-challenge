package com.avenuecode.sergio.challenge.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.avenuecode.sergio.challenge.entity.CustomerOrder;
import com.avenuecode.sergio.challenge.repository.OrderRepository;

@Service
public class OrderService {
	
	@Autowired
	private OrderRepository repository;
	
	@Transactional
	public Integer placeAnOrder(CustomerOrder order) {
		return repository.saveOrder(order);
	}
	
	@Transactional
	public CustomerOrder getOrder(Integer id){
		return repository.getOrder(id);			
		
	}
	
	public void setRepository(OrderRepository repository) {
		this.repository = repository;
	}

	@Transactional
	public List<CustomerOrder> getAllOrders() {
		return repository.getAllOrders();

	}
	
	@Transactional
	public void modifyOrder(CustomerOrder order) {
		repository.updateOrder(order);
	}

}
