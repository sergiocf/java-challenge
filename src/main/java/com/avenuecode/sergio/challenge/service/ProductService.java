package com.avenuecode.sergio.challenge.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.avenuecode.sergio.challenge.entity.Product;
import com.avenuecode.sergio.challenge.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepository repository;
	
	@Transactional 
	public Product getProduct(Integer id) {
		return repository.getProduct(id);	
	}
	
	@Transactional 
	public List<Product> getAllProducts() {
		return repository.getAllProducts();
	}
	
    @Transactional
    public Integer saveProduct(Product product) {
    	return repository.saveProduct(product);
    }	

	public void setRepository(ProductRepository repository) {
		this.repository = repository;
	}
	
	

}
